from flask import Flask, jsonify, request
import datetime
import requests
from requests.exceptions import HTTPError
import json
import sys
import threading


#---------CONFIGURATION-------------
eventManagerURL = "http://127.0.0.1:5000"
#-----------------------------------

flask = Flask(__name__)

# Handle incoming sensor data
@flask.route('/postData', methods=['POST'])
def postDataRequest():

    # Get payload
    data = request.get_json() # type 'dict'

    # Validate data, check if all required keys are present
    # If a key is absent then a status code 400 is returned to client
    requirements = {'location', 'sensor', 'value'}
    for x in requirements:
        if not(x in data):
            return flask.response_class(status = 400) #HTTP Response

    dt = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S") # get current date-time
    data.update({"datetime":dt}) # add date-time to the data posted

    try:
        postData(eventManagerURL+'/insertData', data)
    except:
        print("Exception: ",sys.exc_info()[0])
    return flask.response_class(status = 200) #HTTP Response

# Handle incoming high significance data
@flask.route('/postUrgent', methods=['POST'])
def postUrgent():

    # Get payload
    data = request.get_json() # type 'dict'

    # Validate data, check if all required keys are present
    # If a key is absent then a status code 400 is returned to client
    requirements = {'location', 'sensor', 'value'}
    for x in requirements:
        if not(x in data):
            return flask.response_class(status = 400) #HTTP Response

    dt = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S") # get current date-time
    data.update({"datetime":dt}) # add date-time to the data posted
    data.update({"urgent":"true"}) # add urgent flag to the incoming data

    try:
        postData(eventManagerURL+'/insertData', data)
    except:
        print("Exception: ",sys.exc_info()[0])
    return flask.response_class(status = 200) #HTTP Response

# Receive characteristics of a end-device connected to the network
@flask.route('/actuatorDeployed', methods=['POST'])
def actuatorDeployed():

    # Get payload
    data = request.get_json() # type 'dict'

    # Validate data, check if all required keys are present
    # If a key is absent then a status code 400 is returned to client
    requirements = {'location', 'actuator', 'range'}
    for x in requirements:
        if not(x in data):
            return flask.response_class(status = 400) #HTTP Response

    # Get client's IP address
    ip = request.remote_addr

    # Add ip item to the data to be posted
    data.update({'ip':ip})

    # Post actuator characteristics to Manager
    postData(eventManagerURL+'/actuatorDeployed', data)

    return flask.response_class(status = 200) #HTTP Response

@flask.route('/postCommand', methods=['POST'])
def postCommand():

    # Incoming data consist of 'location' 'actuator' 'value' 'ip'
    data = request.get_json() # type 'dict'

    # URL is like "http://ip:port/route
    URL = 'http://'+data['ip']+':80'+'/actuate'

    # the data to be posted are finally 'location' 'actuator' 'value'
    del data['ip']

    # post the data to the end-device
    postData(URL, data)

    return flask.response_class(status = 200)

# Post a dict as json to a given URL
def postData(URL, data):
    headers = {'Content-type': 'application/json'} # neccessary fot compatibility
    try:
        # the POST is executed the server response is stored
        response = requests.post(url=URL, data=json.dumps(data), headers=headers)
         # If the response was successful, no Exception will be raised
        response.raise_for_status()
        code = response.status_code
        print('HTTP Code:', code)
    except Exception as e:
        print("Exception: ", e)



if __name__ == '__main__':
    threading.Thread(target=flask.run(debug = True, host='0.0.0.0', port = 3000)).start()

#request.remote_addr
#datetime_object = datetime.datetime.now()
#output: 2018-12-19 09:26:03.478039
